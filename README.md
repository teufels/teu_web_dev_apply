# Hi 👋, wir sind die Agentur teufels

## Wir suchen gerade nach Unterstützung für die Webentwicklung.

Super wäre, wenn du Erfahrung mit TYPO3 und/oder Shopware hättest.  
Die Stellenanzeige findest du im PDF.

Außerdem kannst du dich über diesen Link bewerben: [(Senior) Webentwickler (m/w/d)](https://teufels12.softgarden.io/job/10896116/-Senior-Webentwickler-m-w-d-?jobDbPVId=29875842&l=de)

Melde dich bei Fragen einfach bei Nicole Miller unter: [jobs@teufels.com](mail:jobs@teufels.com)


### Hier findest du uns:

## [teufels.com](https://teufels.com/)